package main

import (
	"fmt"
)

func main() {
	fmt.Printf("Result is %d", GetResult())
}

// GetResult Expect get 1
func GetResult() int {
	return 1
}
