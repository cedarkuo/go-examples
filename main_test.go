package main

import (
	"testing"
)

func TestGetResult(t *testing.T) {
	if GetResult() != 1 {
		t.Error("Result is not expected")
	}
}
